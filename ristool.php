<?php

/**
 * RIS file to Web of Science search.
 * Take a RIS export from Mendeley and output some files that you can use to get more data out of Web of Science.
 * Save your papers export as "in.ris" and then run "php ristoids.php". You'll get a bunch of files, some that are useful
 * for an aborted attempt to use the WoS API to perform queries (which need a username/password pair that I've not yet been able 
 * to track down). 
 *
 * The most useful is probably "out_wos_query.txt" which is a query composed of various DOIs that you can use to extract "cited publication" data.
 */

$in = 'in.ris';
$out = 'out_ids.csv';
$out_ssn = 'out_ssns_forjcr.csv';
$out_query = 'out_wos_query.txt';

$f = fopen($in, 'r');
$fcsv = fopen($out, 'w');
$fssn_csv = fopen($out_ssn, 'w');
$fq = fopen($out_query, 'w');


$n = 0;
$record = [];

fputcsv($fcsv, [
	'PMID','DOI','UT','atitle','stitle','vol','issue','spage','issn','year','authors'
]);

fputcsv($fssn_csv, ['ID', 'ISSN']);


while (!feof($f)) {

	$line = fgets($f);
	$sections = explode('  - ', $line, 2);

	if ($sections[0] == 'ER') {
		//print_r($record);

		$record['SN'] = preg_split('/[^a-z0-9\-]+/i', $record['SN'])[0];
		$record['DO'] = str_replace('https://doi.org/', '', preg_split('/\s+/', $record['DO'])[0]);
				

		fputcsv($fcsv, [
			'', // PMID
			$record['DO'], // DOI
			$record['AN'], // UT
			$record['T1'], // atitle // ?
			'', // stitle
			'', // vol
			$record['IS'], // issue
			'', // spage
			trim ($record['SN'], '" '), // issn
			$record['PY'], // year
			$record['A1'], // authors
		]);

		fputcsv($fssn_csv, [
			$n, trim($record['SN'], ' "')			
		]);


		// Fallback - call WoS query
		if (!empty($record['DO']))
			fputs($fq, "DO={$record['DO']} OR ");		
		//else if (!empty($record['SN']))
                //        fputs($fq, "IS={$record['SN']} OR ");
		else
			echo "WARNING: No record ID found for record, skipping\n";
		
		$n++;

		$record = [];
	} else if (!empty($sections[1])) {
		if (isset($record[$sections[0]]))
			$record[$sections[0]] = trim($record[$sections[0]] . ';'. trim($sections[1]), '; ');
		else
			$record[$sections[0]] = trim( $sections[1] );
	}


}



fclose($f);
fclose($fcsv);
fclose($fssn_csv);
fclose($fq);

echo "\n\n $n records processed\n";
