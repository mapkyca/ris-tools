# RIS Tool

This is a quick and dirty tool that adds some ways to add extra information (citations etc) using Web Of Science.

It will take an RIS export produced from Mendeley, and output a number of files. Some of these files are useful 
for using the API (which at time of writing I never got to work), and one file which constructs a query string based on DOI, 
that you can use on the WoS advanced query search on their website.

## Usage
 
1. Save your bibliographic data as a .ris export from Mendeley (or similar) as "in.ris"
2. Run ```php ristool.php```

You will then get a number of output files, cut and paste the contents of "out_wos_query.txt" into the advanced search box (removing the trailing OR, because this tool was hacked together quickly and I didn't tidy this up)

## See

* Author: Marcus Povey <marcus@instruct-eric.eu>
